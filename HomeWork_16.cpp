

#include <iostream>
#include <ctime>

int main()
{
    const int N = 5; // ������ ����������� �������
    int array[N][N];

    // ��������� ������ ���������� i + j
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    // ������� ������ � �������
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }

    // ������� ����������� ����
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    std::cout << "current day: " << buf.tm_mday << std::endl;

    // ������� ������� �� �������
    int numberStr = buf.tm_mday % N;
    std::cout << "string index: " << numberStr << std::endl;

    int sum = 0;
    for (int i = 0; i < N; i++)
    {
        sum += array[numberStr][i];
    }
    std::cout << "sum: " << sum << std::endl;

    return 0;
}
